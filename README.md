# XAVIER

Xavier is a programming language that aims to encompass favorable attributes
from different programming languages, namely:
- *Efficient* as compiled C and C++
- *Readable* as python
- *Concise* as python
- *Compact* as Lua's interpreter
- *Singular* as static C and C++

To achieve this, we must first understand what makes each of these
attributes possible:
- *Efficiency* comes from the program being compiled into machine code
  (a la Assembly) 
- *Readablility* comes from the use of human-readable code; sometimes
  substitutes symboles for words
- *Concisness* comes from using shorter diction and proper break-down,
  so as to prevent run-on lines
- *Compactness* comes from low complexity and/or low required overhead
- *Singularity* comes from ensuring that all required resources are in
  one place, in this case, a file

To achieve each of these goals, here is a breakdown of Xavier approaches
each case:

## Efficiency
In the case of C-like languages, the (basic) schema for compiling is
as follows:
```
[C Source] --> [Assembly] --> [Binary]
```
While the ideal path for a copiled language would be to go directly
from source code to binary, it can be rather difficult, and as such,
would be better suited to compiling itself into another compiling
language first. With that in mind, there are two major options:
- C
- C++

While C++ offers more flexibility in terms of Objects and Generics,
the requirement of an extra library can violate the values
*Compactness* and *Singularity*. In either case, however, adapters
will be required to handle conversions from Xavier to C/C++, either
in the form of preprocessor translations or a library. Currently,
the verion 6.0.28 of libstdc++ is about 1.9M, which is not very
small. Ironically, this can be overcome when we try to acheive
*Compactness* and *Singularity* by using the '-static-libstdc++'
flags in the compiler. This both unbridles the 1.9M library the 
system would require and allows for compiling in **only** what's
necessary.

Another factor to acount for is compile-time and flexibility.
By building Xavier on top of another popular and well-supported
language, such as C++, we can leverage long-supported compilers
and ones which support cross-compilation easily.

## *Readablility*

